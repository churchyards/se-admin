import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router, Route} from 'react-router-dom'

import './index.css';
import * as serviceWorker from './serviceWorker';
import {config} from './config.js';

import App from './App';
import {ShippingDocuments} from "./components/ShippingDocuments/ShippingDocuments";
import {Dashboard} from "./components/Dashboard/Dashboard";
import {AppSettings} from "./components/AppSettings/AppSettings";

const routing = (
  <Router basename={config.app_base}>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/vrachtbrief" component={ShippingDocuments} />
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/instellingen" component={AppSettings} />
    </div>
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
