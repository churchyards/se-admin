import React from 'react';
import styled from '@emotion/styled'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 300px;
  height: 300px;
  background-color: white;
  border-radius: 20px;
  color: black;
`;

const Header = styled.div`
  height: 28px;
  padding: 10px 20px 0px 20px;
  border-bottom: 1px solid lightgrey;
  text-align: left;
  font-size: 1em;
  font-weight: bold;
`;

const Body = styled.div`
  padding: 10px;
`;

const FieldGroupComponent = (props) => {
  return (
    <Container>
      <Header>{props.title}</Header>
      <Body>{props.children}</Body>
    </Container>
  )
};

export const FieldGroup = FieldGroupComponent;
