import React from 'react';
import styled from "@emotion/styled";

import {Logo} from "../Icons/Logo/Logo";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  height: 50px;
  background-color: #F18919;
`;

const Title = styled.span`
  padding: 15px 0 0 10px;
  color: white;
  font-weight: bold;
  font-size: 1em;
`;

const Icon = styled.div`
    padding: 4px 10px 0 0px;
`;

const HeaderComponent = (props) => {
  return (
    <Container>
      <Title>{props.title}</Title>
      <Icon>
        <Logo width="128" height="45" />
      </Icon>
    </Container>
  )
}

export const Header = HeaderComponent

