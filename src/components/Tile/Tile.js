import React from 'react';
import styled from '@emotion/styled'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 190px;
  height: 190px;
  margin: 20px;
  padding: 32px;
  text-align: center;
  background-color: white;
  font-size: 24px;
  border-radius: 20px;
  color: black;
  font-weight: bold;
  &:hover {
    background-color: grey;
    cursor: pointer;
  }
`;

const TileComponent = (props) => {
  const nav = () => document.location.href = props.target;
  return (
    <Container onClick={nav}>
      <div>{props.children}</div>
      <h4>{props.title}</h4>
    </Container>
  )
};

export const Tile = TileComponent;
