import React from 'react';
import {Header} from "../Header/Header";

const AppSettingsComponent = () => {
  return (
    <div>
      <Header title="Instellingen" />
    </div>
  )
};

export const AppSettings = AppSettingsComponent;
