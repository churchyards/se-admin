import React from 'react';
import styled from '@emotion/styled'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding: 2px;
  background-color: white;
  color: black;
`;

const Label = styled.span`
  width: 120px;
  font-size: 0.8em;
`;

const StyledInput = styled.input`
  width: 64px;
  // border-width:0px;
  // border:none;
`;

const Inputs = styled.div`
  display: flex;
  flex-display: row;
  justify-content: space-between;
  width: 146px;
`;

const TextFieldDuoComponent = (props) => {
  return (
    <Container>
      {
        props.label ? <Label>{props.label}</Label> : null
      }
      <Inputs>
        <StyledInput
          type="text"
          name={props.name1}
          ref={props.register}
        >
        </StyledInput>
        <StyledInput
          type="text"
          name={props.name2}
          ref={props.register}
        >
        </StyledInput>
      </Inputs>
    </Container>
  )
};

export const TextFieldDuo = TextFieldDuoComponent;
