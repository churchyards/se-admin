import React from 'react';
import styled from '@emotion/styled'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding: 2px;
  background-color: white;
  color: black;
`;

const Label = styled.span`
  width: 120px;
  font-size: 0.8em;
`;

const StyledInput = styled.input`
  width: 140px;
`;

const TextFieldComponent = (props) => {
  return (
    <Container>
      <Label>{props.label}</Label>
      <StyledInput
        type="text"
        name={props.name}
        ref={props.register}
      >
      </StyledInput>
    </Container>
  )
};

export const TextField = TextFieldComponent;
