import React from 'react';
import styled from '@emotion/styled'

import {config} from '../../config.js';

import {Tile} from "../Tile/Tile";
import {ShippingIcon} from "../Icons/ShippingIcon/ShippingIcon";
import {ChartIcon} from "../Icons/ChartIcon/ChartIcon";
import {AppSettingsIcon} from "../Icons/AppSettingsIcon/AppSettingsIcon";
import {Header} from "../Header/Header";

const BASE_NAME = config.app_base;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 1200px;
  background-color: lightgrey;
`;

const Tiles = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 50px;
`;

const OverviewComponent = () => {
  return (
    <Container>
      <Header />

      <Tiles>
        <Tile title="Vrachtbrief" target={`${BASE_NAME}/#/vrachtbrief`}>
          <ShippingIcon size="32" color="black" />
        </Tile>
        <Tile title="Dashboard" target={`${BASE_NAME}/#/dashboard`}>
          <ChartIcon size="32" color="black" />
        </Tile>
        <Tile title="Instellingen" target={`${BASE_NAME}/#/instellingen`}>
          <AppSettingsIcon size="32" color="black" />
        </Tile>
      </Tiles>
    </Container>
  );
};

export const Overview = OverviewComponent;
