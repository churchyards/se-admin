import React from 'react';
import { useForm } from 'react-hook-form';
import styled from '@emotion/styled'
import axios from 'axios';
import moment from "moment";

import { config } from '../../config.js';

import {Header} from "../Header/Header";
import {FieldGroup} from "../FieldGroup/FieldGroup";
import {TextField} from "../TextField/TextField";
import {TextFieldDuo} from "../TextFieldDuo/TextFieldDuo";

const APP_BASE = config.app_base;
const API_URL = config.api_url;

const Container = styled.div`
  background-color: lightgrey;
  height: 1200px;
`;

const Tiles = styled.div`
  display: flex; 
  flex-direction: row;
  justify-content: flex-start;
  flex-wrap: wrap;
  padding: 10px;
`;

const Tile = styled.div`
  margin: 10px;
`;

const ButtonBar = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin: 0 20px 0;
`;

const StyledButton = styled.button`
  width: 100px;
  height: 40px;
  margin-left: 10px;
  font-size: 1em;
  font-weight: bold;
  border-radius: 20px;
  background-color: ${ props => props.primary ? '#F18919' : 'darkgrey' };
  color: white;
    &:hover {
    background-color: grey;
    cursor: pointer;
  }
`;

const ShippingDocumentsComponent = () => {
  const { register, handleSubmit } = useForm();

  const axiosConfig = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/pdf'
    },
    responseType: 'blob',
  };

  const onSubmit = data => {
    console.log(data);
    axios.post(API_URL, data, axiosConfig)
      .then( response => {
        const file = new Blob([response.data], {type: 'application/pdf'});
        // process to auto download it
        const fileURL = URL.createObjectURL(file);
        const link = document.createElement('a');
        link.href = fileURL;
        link.download = `vrachtbrief_${moment().format('YYYY-MM-DD_HHmm')}.pdf`;
        link.click();
      })
      .catch( error => console.log(error) );
  };

  const onCancel = () => document.location.href = APP_BASE;

  return (
    <Container>
      <Header title="Vrachtbrief"/>
      <Tiles>
        <Tile>
          <FieldGroup title="Klant">
            <TextField label="Bedrijfsnaam" name="companyName" register={register} />
            <TextField label="Contactpersoon" name="contactPerson" register={register} />
            <TextField label="Telefoonnummer" name="phoneNumber" register={register} />
            <TextField label="Faxnummer" name="faxNumber" register={register} />
            <TextField label="E-mailadres" name="emailAddress" register={register} />
          </FieldGroup>
        </Tile>
        <Tile>
          <FieldGroup title="Referentie">
            <TextField label="AWB" name="awb" register={register} />
            <TextField label="Referentie/PO" name="reference" register={register} />
            <TextField label="Bezorgdatum" name="deliveryDueDate" register={register} />
            <TextField label="Bevestiging" name="deliveryConfirmation" register={register} />
            <TextField label="Ophaaldatum" name="pickupDueDate" register={register} />
          </FieldGroup>
        </Tile>
        <Tile>
          <FieldGroup title="Goederen">
            <TextField label="Omschrijving" name="goodsDescription" register={register} />
            <TextField label="Aantal colli" name="numberOfPackages" register={register} />
            <TextField label="Gewicht" name="weight" register={register} />
            <TextField label="Bijzonderheden" name="goodsDetails" register={register} />
          </FieldGroup>
        </Tile>
        <Tile>
          <FieldGroup title="Gewenste service">
            <TextField label="Service" name="serviceName" register={register} />
            <TextField label="Opties" name="optie1" register={register} />
            <TextField label="" name="optie2" register={register} />
            <TextField label="" name="optie3" register={register} />
            <TextField label="" name="optie4" register={register} />
            <TextField label="" name="optie5" register={register} />
            <TextField label="" name="optie6" register={register} />
          </FieldGroup>
        </Tile>
        <Tile>
          <FieldGroup title="Ophaaladres">
            <TextField label="Bedrijfsnaam" name="pickupCompanyName" register={register} />
            <TextField label="Straat" name="pickupStreet" register={register} />
            <TextFieldDuo label="Huisnummer" name1="pickupHouseNumber" name2="pickupHouseNumberSuffix" register={register} />
            <TextField label="Postcode" name="pickupPostalCode" register={register} />
            <TextField label="Plaats" name="pickupCity" register={register} />
            <TextField label="Land" name="pickupCountry" register={register} />
            <TextField label="Contactpersoon" name="pickupContactPerson" register={register} />
            <TextField label="Telefoonnummer" name="pickupPhoneNumber" register={register} />
            <TextField label="E-mailadres" name="pickupEmailAddress" register={register} />
          </FieldGroup>
        </Tile>
        <Tile>
          <FieldGroup title="Bezorgadres">
            <TextField label="Bedrijfsnaam" name="deliveryCompanyName" register={register} />
            <TextField label="Straat" name="deliveryStreet" register={register} />
            <TextFieldDuo label="Huisnummer" name1="deliveryHouseNumber" name2="deliveryHouseNumberSuffix" register={register} />
            <TextField label="Postcode" name="deliveryPostalCode" register={register} />
            <TextField label="Plaats" name="deliveryCity" register={register} />
            <TextField label="Land" name="deliveryCountry" register={register} />
            <TextField label="Contactpersoon" name="deliveryContactPerson" register={register} />
            <TextField label="Telefoonnummer" name="deliveryPhoneNumber" register={register} />
            <TextField label="E-mailadres" name="deliveryEmailAddress" register={register} />
          </FieldGroup>
        </Tile>
      </Tiles>
      <ButtonBar>
        <StyledButton onClick={onCancel}>Annuleren</StyledButton>
        <StyledButton primary onClick={handleSubmit(onSubmit)}>Maak PDF</StyledButton>
      </ButtonBar>
    </Container>
  )
};

export const ShippingDocuments = ShippingDocumentsComponent;
