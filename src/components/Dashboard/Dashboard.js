import React from 'react';
import {Header} from "../Header/Header";

const DashboardComponent = () => {
  return (
    <div>
      <Header title="Dashboard"/>
    </div>
  )
};

export const Dashboard = DashboardComponent;
